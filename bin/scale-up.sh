#!/bin/sh
# uncomment additional instances
sed -i -E "s/#server ff-drift-php_driftphp_([234])/server ff-drift-php_driftphp_\1/g" nginx/driftphp.conf

# scale up driftphp service
docker-compose up -d --scale driftphp=4 driftphp

# restart nginx container to enable round-robin
docker-compose stop nginx && docker-compose up -d nginx

