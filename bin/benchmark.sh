#!/bin/sh

concurrency=${1:-50}
requests=${2:-1000}
domain=${3:-driftphp.test}

ab -c"${concurrency}" -n"${requests}" "http://${domain}/"
