default: symfony driftphp
	docker-compose up -d --build

symfony:
	symfony new symfony --no-git
	cp examples/symfony/Dockerfile symfony/
	cd symfony && composer req annot

driftphp:
	composer create-project drift/skeleton -sdev --remove-vcs driftphp
	cp -n examples/driftphp/server-entrypoint.sh driftphp/docker/server-entrypoint.sh

build:
	docker-compose up -d --build

up:
	docker-compose up -d

restart:
	docker-compose restart

scale-up:
	sh ./bin/scale-up.sh

scale-down:
	sh ./bin/scale-down.sh

http-request:
	cd driftphp && composer require clue/buzz-react:^2.9
	cd symfony && composer req http-client
	cp examples/http-request/DriftDefaultController.php driftphp/src/Controller/DefaultController.php
	cp examples/http-request/SymfonyDefaultController.php symfony/src/Controller/DefaultController.php
	$(MAKE) restart

mysql:
	cd driftphp && composer require drift/dbal-bundle react/mysql
	cp examples/mysql/Controller/*.php driftphp/src/Controller/
	cp -r examples/mysql/DBAL driftphp/src/
	cp -r examples/mysql/Domain driftphp/src/
	cat examples/mysql/routes.yml | sed '0,/^/s//\n/' >> driftphp/Drift/config/routes.yml
	echo "18i\    Drift\\\\\\DBAL\\\\\\DBALBundle::class => ['all' => true]," | sed -i -f- driftphp/Drift/config/bundles.php
	sed -n 2,10p examples/mysql/services.yaml | sed 's/ /\\ /; s/^/12i /g' | sed -i -f- driftphp/Drift/config/services.yml
	sed -n 14,16p examples/mysql/services.yaml | sed '0,/^/s//\n/' >> driftphp/Drift/config/services.yml
	$(MAKE) restart-drift

redis:
	cd driftphp && composer require drift/redis-bundle
	cp examples/cache/Controller/*.php driftphp/src/Controller/
	cp -r examples/cache/Cache driftphp/src/
	echo "18i\    Drift\\\\\\Redis\\\\\\RedisBundle::class => ['all' => true]," | sed -i -f- driftphp/Drift/config/bundles.php
	cat examples/cache/routes.yml | sed '0,/^/s//\n/' >> driftphp/Drift/config/routes.yml
	sed -n 2,8p examples/cache/services.yaml | sed 's/ /\\ /; s/^/12i /g' | sed -i -f- driftphp/Drift/config/services.yml
	sed -n 15,17p examples/cache/services.yaml | sed '0,/^/s//\n/' >> driftphp/Drift/config/services.yml
	$(MAKE) restart-drift

twig:
	cd driftphp && composer require drift/twig-bundle
	cp examples/twig/Controller/*.php driftphp/src/Controller/
	cp -r examples/twig/views driftphp/Drift
	echo "18i\    Drift\\\\\\Twig\\\\\\TwigBundle::class => ['all' => true]," | sed -i -f- driftphp/Drift/config/bundles.php
	cat examples/twig/routes.yml | sed '0,/^/s//\n/' >> driftphp/Drift/config/routes.yml
	$(MAKE) restart-drift

event-dispatcher:
	cp examples/event-dispatcher/Controller/*.php driftphp/src/Controller/
	cp -r examples/event-dispatcher/Domain/Event driftphp/src/Domain/
	cat examples/event-dispatcher/routes.yml | sed '0,/^/s//\n/' >> driftphp/Drift/config/routes.yml
	sed -n 4,5p examples/event-dispatcher/services.yml | sed '0,/^/s//\n/' >> driftphp/Drift/config/services.yml
	$(MAKE) restart-drift

restart-drift:
	docker-compose restart driftphp

clean:
	docker-compose down --remove-orphans --volumes
	sudo rm -rf symfony
	sudo rm -rf driftphp

