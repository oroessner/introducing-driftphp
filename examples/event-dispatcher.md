# Dispatch Events eventually

## DriftPHP

This example has a dependency to the mysql example!

* [GetFakeTodoListAction](event-dispatcher/Controller/GetFakeTodoListAction.php)
* copy the contents of the [Domain](event-dispatcher/Domain) folder to the existing [src/Domain](../driftphp/src/Domain) folder.
* adopt [services.yml](../driftphp/Drift/config/services.yml) and [routes.yml](../driftphp/Drift/config/routes.yml) accordingly.
