# Talk to a MySQL Database

## DriftPHP

`composer require drift/dbal-bundle react/mysql`

* [Default Controller](mysql/Controller/DefaultController.php)
* [NewTodoAction](mysql/Controller/NewTodoAction.php)
* copy all subfolders from the [mysql folder](./mysql) to [src](../driftphp/src).
* add `Drift\DBAL\DBALBundle::class => ['all' => true],` to [bundles.php](../driftphp/Drift/config/bundles.php).
* adopt [services.yml](../driftphp/Drift/config/services.yml) and [routes.yml](../driftphp/Drift/config/routes.yml) accordingly.
