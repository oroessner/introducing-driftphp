<?php
declare(strict_types=1);

namespace App\Controller;

use App\Cache\RedisRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class PutCacheKeyAction
{
    private RedisRepository $repository;

    public function __construct(RedisRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(string $key, string $value)
    {
        return $this->repository
            ->putString($key, $value)
            ->then(fn(string $response) => new JsonResponse(['status' => $response], Response::HTTP_CREATED));
    }
}
