<?php
declare(strict_types=1);

namespace App\Controller;

use App\Cache\RedisRepository;
use Symfony\Component\HttpFoundation\JsonResponse;

final class GetCacheKeysAction
{
    private RedisRepository $repository;

    public function __construct(RedisRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke()
    {
        return $this->repository
            ->keys()
            ->then(fn(array $keys) => new JsonResponse($keys));
    }
}
