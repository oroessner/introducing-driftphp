<?php
declare(strict_types=1);

namespace App\Cache;

use Clue\React\Redis\Client;
use React\Promise\PromiseInterface;

final class RedisRepository
{
    private Client $client;

    public function __construct(Client $defaultClient)
    {
        $this->client = $defaultClient;
    }

    public function keys(): PromiseInterface
    {
        return $this->client->keys('*');
    }

    public function putString(string $key, string $value): PromiseInterface
    {
        return $this->client->set($key, $value);
    }
}
