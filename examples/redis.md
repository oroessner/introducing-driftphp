# Talk to a Redis Cache

## DriftPHP

`composer require drift/redis-bundle`

* [GetCacheKeysAction](cache/Controller/GetCacheKeysAction.php)
* [PutCacheKeyAction](cache/Controller/PutCacheKeyAction.php)
* copy all subfolders from the [cache folder](./cache) to [src](../driftphp/src).
* add `Drift\Redis\RedisBundle::class => ['all' => true],` to [bundles.php](../driftphp/Drift/config/bundles.php).
* adopt [services.yml](../driftphp/Drift/config/services.yml) and [routes.yml](../driftphp/Drift/config/routes.yml) accordingly.
