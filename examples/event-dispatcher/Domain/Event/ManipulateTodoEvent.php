<?php

declare(strict_types=1);

namespace Domain\Event;

use Domain\Todo;

final class ManipulateTodoEvent
{
    private Todo $todo;

    private function __construct(Todo $todo)
    {
        $this->todo = $todo;
    }

    public static function create(Todo $todo): self
    {
        return new self($todo);
    }

    public function todo(): Todo
    {
        return $this->todo;
    }
}
