<?php

declare(strict_types=1);

namespace Domain\Event;

use React\Promise\PromiseInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use function React\Promise\resolve;

/**
 * Class TodoRandomizer
 *
 * @package App\Domain\Event
 */
final class TodoRandomizer implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): iterable
    {
        yield ManipulateTodoEvent::class => 'randomizeResolve';
    }

    public function randomizeResolve(ManipulateTodoEvent $event): PromiseInterface
    {
        return resolve()
            ->then(
                static function () use ($event) {
                    if (5 < random_int(1, 10)) {
                        $event->todo()->resolve();
                    }
                }
            );
    }
}
