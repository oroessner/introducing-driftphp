<?php

declare(strict_types=1);

namespace App\Controller;

use Closure;
use Domain\Event\ManipulateTodoEvent;
use Domain\Todo;
use Infrastructure\DBAL\TodoRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

final class GetFakeTodoListAction
{
    private TodoRepository $repository;

    private EventDispatcherInterface $eventDispatcher;

    public function __construct(TodoRepository $repository, EventDispatcherInterface $eventDispatcher)
    {
        $this->repository      = $repository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke()
    {
        return $this->repository
            ->findAll()
            ->then(Closure::fromCallable([$this, 'createTodos']))
            ->then(Closure::fromCallable([$this, 'fetchTodos']))
            ->then(fn(array $todos) => new JsonResponse($todos));
    }

    /**
     * @return Todo[]
     */
    private function createTodos(array $todos): array
    {
        return array_map(fn(array $todo) => Todo::fromResult($todo), $todos);
    }

    /**
     * @param Todo[] $todos
     *
     * @return Todo[]
     */
    private function fetchTodos(array $todos): array
    {
        foreach ($todos as $todo) {
            $this->eventDispatcher->dispatch(ManipulateTodoEvent::create($todo));
        }

        return $todos;
    }
}
