<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class DefaultController.
 */
final class DefaultController
{
    private HttpClientInterface $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @Route("/")
     */
    public function __invoke(Request $request)
    {
        $response = $this->client->request('GET', 'http://whoami/?wait=100ms');

        return new Response($response->getContent(), $response->getStatusCode());
    }
}
