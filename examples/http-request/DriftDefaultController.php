<?php declare(strict_types=1);

namespace App\Controller;

use Clue\React\Buzz\Browser;
use Psr\Http\Message\ResponseInterface;
use React\EventLoop\LoopInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController.
 *
 * You can run this action by making `curl` to /
 */
final class DefaultController
{
    /**
     * @var \React\EventLoop\LoopInterface
     */
    private LoopInterface $loop;

    public function __construct(LoopInterface $loop)
    {
        $this->loop = $loop;
    }

    /**
     * Default path.
     */
    public function __invoke(Request $request)
    {
        $client = new Browser($this->loop);

        return $client
            ->get('http://whoami/?wait=100ms')
            ->then(fn(ResponseInterface $response) => new Response(
                (string)$response->getBody(),
                $response->getStatusCode()
            ));
    }
}
