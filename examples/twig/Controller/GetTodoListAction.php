<?php
declare(strict_types=1);

namespace App\Controller;

use Drift\Twig\Controller\RenderableController;
use Infrastructure\DBAL\TodoRepository;

final class GetTodoListAction implements RenderableController
{
    private TodoRepository $todoRepository;

    public function __construct(TodoRepository $todoRepository)
    {
        $this->todoRepository = $todoRepository;
    }

    public static function getTemplatePath(): string
    {
        return 'todo/index.html.twig';
    }

    public function __invoke(): array
    {
        return [
            'todos' => $this->todoRepository
                ->findAll()
                ->then(fn(array $results) => $results),
        ];
    }
}
