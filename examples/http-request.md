# Making an HTTP call

## DriftPHP

`composer require clue/buzz-react:^2.9`

* [Default Controller](http-request/DriftDefaultController.php)

# Symfony

`composer req http-client`

* [Default Controller](http-request/SymfonyDefaultController.php)
