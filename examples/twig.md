# Render Twig Templates

## DriftPHP

This example has a dependency to the mysql example!

`composer require drift/twig-bundle`

* [GetTodoListAction](twig/Controller/GetTodoListAction.php)
* copy the [view](./twig/views) folder to the [Drift](../driftphp/Drift) folder.
* add `Drift\Twig\TwigBundle::class => ['all' => true],` to [bundles.php](../driftphp/Drift/config/bundles.php).
* adopt [services.yml](../driftphp/Drift/config/services.yml) and [routes.yml](../driftphp/Drift/config/routes.yml) accordingly.
