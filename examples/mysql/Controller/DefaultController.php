<?php declare(strict_types=1);

namespace App\Controller;

use Domain\Todo;
use Infrastructure\DBAL\TodoRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController.
 *
 * You can run this action by making `curl` to /
 */
final class DefaultController
{
    /**
     * @var \Infrastructure\DBAL\TodoRepository
     */
    private TodoRepository $todoRepository;

    public function __construct(TodoRepository $todoRepository)
    {
        $this->todoRepository = $todoRepository;
    }

    /**
     * Default path.
     */
    public function __invoke(Request $request)
    {
        return $this->todoRepository
            ->findAll()
            ->then(
                fn(array $todos) => new JsonResponse(
                    array_map(fn(array $todo) => Todo::fromResult($todo), $todos)
                )
            );
    }
}
