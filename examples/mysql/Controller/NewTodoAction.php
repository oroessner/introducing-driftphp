<?php
declare(strict_types=1);

namespace App\Controller;

use DateTimeImmutable;
use Domain\Todo;
use Drift\DBAL\Result;
use Infrastructure\DBAL\TodoRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class NewTodoAction
 *
 * @package App\Controller
 */
final class NewTodoAction
{
    /**
     * @var \Infrastructure\DBAL\TodoRepository
     */
    private TodoRepository $repository;

    public function __construct(TodoRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke()
    {
        $later = new DateTimeImmutable('+1 month');
        $todo  = Todo::withTask(sprintf('Do something at %s', $later->format('Y-m-d')));

        return $this->repository
            ->create($todo)
            ->then(fn(Result $result) => new JsonResponse(
                $todo->withId($result->getLastInsertedId()),
                Response::HTTP_CREATED
            ));
    }
}
