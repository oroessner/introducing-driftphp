<?php
declare(strict_types=1);

namespace Domain;

use DateTimeImmutable;
use DateTimeInterface;
use JsonSerializable;

use function filter_var;

/**
 * Class Todo
 *
 * @package Domain
 */
final class Todo implements JsonSerializable
{
    private const DATE_FORMAT = 'Y-m-d H:i:s';

    private ?int $id = null;

    private string $task;

    private DateTimeInterface $createdAt;

    private bool $resolved = false;

    private function __construct()
    {
        $this->createdAt = new DateTimeImmutable();
    }

    public static function withTask(string $task): self
    {
        $todo       = new self();
        $todo->task = $task;

        return $todo;
    }

    public static function fromResult(array $data): self
    {
        $todo            = new self();
        $todo->id        = (int)$data['id'];
        $todo->task      = (string)$data['task'];
        $todo->createdAt = DateTimeImmutable::createFromFormat(self::DATE_FORMAT, $data['created_at']);
        $todo->resolved  = filter_var($data['resolved'], FILTER_VALIDATE_BOOLEAN);

        return $todo;
    }

    public function withId(int $id): self
    {
        $withId     = clone $this;
        $withId->id = $id;

        return $withId;
    }

    public function jsonSerialize()
    {
        return [
            'id'         => $this->id,
            'task'       => $this->task,
            'created_at' => $this->createdAt->format(self::DATE_FORMAT),
            'resolved'   => $this->resolved,
        ];
    }

    public function resolve(): void
    {
        $this->resolved = true;
    }
}
