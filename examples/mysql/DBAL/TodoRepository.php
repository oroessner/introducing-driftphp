<?php
declare(strict_types=1);

namespace Infrastructure\DBAL;

use Domain\Todo;
use Drift\DBAL\Connection;
use React\Promise\PromiseInterface;

final class TodoRepository
{
    private const TABLE = 'todos';

    private Connection $defaultConnection;

    public function __construct(Connection $defaultConnection)
    {
        $this->defaultConnection = $defaultConnection;
    }

    public function findAll():PromiseInterface
    {
        return $this->defaultConnection
            ->findBy(self::TABLE);
    }

    public function create(Todo $todo):PromiseInterface
    {
        return $this->defaultConnection
            ->insert(self::TABLE, $todo->jsonSerialize());
    }
}
