# $ whoami

* Ole Rößner
* married, father
* neusta GmbH (Bremen)
* Coding, Coaching and Consulting
* Organizer of [PHPUGHB](https://phpughb.github.io/)
* Symfony Enthusiast
* Clean Code Evangelist
* Former DJ

## Follow Me

* [@djbasster](https://twitter.com/djbasster)
* [@oroessner@mastodon.social](https://mastodon.social/@oroessner)
* [Basster @ Github](https://github.com/Basster)
* [basster @ Mixcloud](https://www.mixcloud.com/basster/)
* [o.roessner@neusta.de](mailto:o.roessner@neusta.de)
