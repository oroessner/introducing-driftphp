# About DriftPHP

All taken from the official [DriftPHP docs](https://driftphp.io/)

## Let's talk about...

* What does the Symfony Kernel do?
  > Given a Request, give me a Response
* What does an HTTP-Server do?[¹][1]
  * Your Nginx takes the HTTP Request.
  * The Http Request is passed to PHP-FPM and is interpreted by PHP-FPM.
  * Interpreted means, basically, that your public/index.php file will be executed.
  * One kernel is instanced, and a new Symfony Request is created.
  * The Symfony Request is handled, and a new Response is generated.
  * The Response is passed through the Nginx and returned to the final user.

## [ReactPHP](https://reactphp.org/)

> ReactPHP is a low-level library for event-driven programming in PHP. At its core is an event loop, on top of which it provides low-level utilities, such as: Streams abstraction, async DNS resolver, network client/server, HTTP client/server and interaction with processes. Third-party libraries can use these components to create async network clients/servers and more.

* [Just turned 8](https://twitter.com/another_clue/status/1281958432816472064) and released Version v1.0.0 on July, 11th 2020.

### Async?

> Your code that includes, somewhere, a result of an I/O operation, will return something. **Eventually**.

### Non Blocking?

> An **event loop** treats all your **Promises** in a non-blocking way.

### Promise?

> A class that can be fulfilled (value?) or rejected (Exception)

* A promise is fulfilled when it returns nothing, a simple value or another Fulfilled promise
* You can concat Promises in order to make your code pretty easy to read

### Event Loop?

> The loop can manage as many promises as you can, even generated from other libraries (Redis, Mysql...)

Simplified Event Loop:

```
while (queue.waitForMessage()) {
    queue.processNextMessage();
}
```

### ReactPHP Server?

Glues all this together and provides a continuously running server listening on a socket.

## DriftPHP

DriftPHP contains of three main components

### HTTP Kernel

> Overwrites the regular and blocking Symfony Kernel to a new one, inspired by the original, but adapted to work on top of Promises. So everything here will be non-blocking, and everything will happen _eventually_.

### HTTP Server

Creates an AsyncKernel (once!) and serves it through a ReactPHP Server instance.

### Skeleton

Starting point for a new DriftPHP application. A little like the Symfony skeleton, but on the other hand, totally different. But... its fully optional!

### Components & Bundles

* [HTTP-Kernel](https://github.com/driftphp/http-kernel): `drift/http-kernel`
* [HTTP-Server](https://github.com/driftphp/server): `driftphp/server`
* [Command Bus](https://github.com/driftphp/command-bus-bundle): `drift/command-bus`
* [ReactPHP functions](https://github.com/driftphp/reactphp-functions): `drift/react-functions`
* [Redis adapter](https://github.com/driftphp/redis-bundle): `drift/redis-bundle`
* [DBAL adapter](https://github.com/driftphp/dbal-bundle): `drift/dbal-bundle`
* [AMQP adapter](https://github.com/driftphp/amqp-bundle): `drift/amqp-bundle`
* [Twig adapter](https://github.com/driftphp/twig-bundle): `drift/twig-bundle`
* [Websocket adapter](https://github.com/driftphp/websocket-bundle): `drift/websocket-bundle`
* and some more...

⚠️ There is also a [Demo](https://github.com/driftphp/demo) showcasing almost all components in one project!

> DriftPHP is still in a development process and thus it shouldn’t be considered yet as a production-ready framework. The official documentation is also still in progress. But it is an interesting approach for writing asynchronous PHP applications. Where our application also behaves as a non-blocking asynchronous web-server.

---

Let's get our hands dirty!

[1]: https://youtu.be/jNdzwRdEEYQ?t=110 "Youtube: HTTP Pipeline and PHP as a Restaurant analogy by Andrew Carter @ SymfonyCon Berlin 2016"
