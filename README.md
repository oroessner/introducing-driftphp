# Introduction DriftPHP - Asynchronous Symfony Applications

![DriftPHP Logo](https://driftphp.io/_media/driftphp.png)

This repository contains a docker-compose stack configuration to run a [Symfony](https://symfony.com/) app alongside a [DriftPHP](https://driftphp.io/) server for demonstration purposes.

## Who am I?

[Ole Rößner](slides/00-whoami.md)

## Requirements

* [Docker](https://docs.docker.com/get-docker/) and [Docker-Compose](https://docs.docker.com/compose/install/)
* [Composer](https://getcomposer.org/download/)
* [Symfony CLI](https://symfony.com/download)
* [Apache Benchmark](https://httpd.apache.org/docs/2.4/programs/ab.html)

## Get Started

* Register local test domains:

  Add the following line to your `/etc/hosts`:

  ```
  127.0.0.1 symfony.test driftphp.test
  ```

* Bootstrap Symfony and DriftPHP skeletons:

  ```bash
  # create symfony skeleton with annotations
  symfony new symfony --no-git
  cp examples/symfony/Dockerfile symfony/
  cd symfony && composer req annot && cd ..
  # alternatively: make symfony

  # create driftphp skeleton
  composer create-project drift/skeleton -sdev --remove-vcs driftphp
  cp -n examples/driftphp/server-entrypoint.sh driftphp/docker/server-entrypoint.sh
  # alternatively: make driftphp
  ```

* Start Docker-Compose stack

  ```bash
  docker-compose up -d --build
  # alternatively: make build
  ```

## Scaling

To scale the driftphp app behind nginx I've prepared two scripts to ease the process. The scripts manipulate the nginx config for the driftphp app, scale the service and restart the nginx container to adopt the changes:

* [`bin/scale-up.sh`](./bin/scale-up.sh) scales up the driftphp app to 4 replicas.
* [`bin/scale-down.sh`](./bin/scale-down.sh) scales the driftphp app back down to 1 replica.

You can use `make scale-up` and `make scale-down` alternatively.

## Benchmarking

To test both frameworks I've prepared a wrapper script around Apache Benchmark [`bin/benchmark.sh`](./bin/benchmark.sh).

It supports three consecutive arguments:

* concurrency -> defaults to `50`
* requests -> defaults to `1000`
* domain -> defaults to `driftphp.test`

Full example: `./bin/benchmark.sh 10 100 symfony.test`

## Building up the DriftPHP example for this session

A little introduction about DriftPHP and its basics can be found [here](slides/01-driftphp.md).

I've prepared some examples around DriftPHP, best followed in this order:

1. [http-request](examples/http-request.md) (`make http-request`)
1. [mysql](examples/mysql.md) (`make mysql`)
1. [redis](examples/redis.md) (`make redis`)
1. [twig](examples/twig.md) (`make twig`)
1. [event-dispatcher](examples/event-dispatcher.md) (`make event-dispatcher`)

## Sources

* [Symfony and ReactPHP Series @ Medium](https://medium.com/@apisearch/symfony-and-reactphp-series-82082167f6fb)
* [When symfony met promises by Marc Morena](https://es.slideshare.net/MarcMorera/when-symfony-met-promises-167235900)
* [DriftPHP: Quick Start](https://sergeyzhuk.me/2020/05/08/driftphp-quick-start/)

## License

[![Creative Commons License](https://i.creativecommons.org/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/)  \
This work is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/)
